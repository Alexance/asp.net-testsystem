﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TestSystem;
using TestSystem.Models;
using TestSystem.Models.Transfers;

namespace TestSystem.Controllers
{
    public class TestsController : ApiController
    {
        private TestContext db = new TestContext();

        // GET: api/Tests
        public IEnumerable<TestItem> GetTests()
        {
            List<TestItem> list = db.Tests.Select(x => new TestItem { id = x.Id, name = x.name }).ToList();

            return list;
        }

        // GET: api/Tests/5
        [ResponseType(typeof(Test))]
        public List<Question> GetTest(int id)
        {
            List<Question> allQuestionsData = db.Questions.Where(x => x.TestId == id).ToList();
            List<Question> questions = allQuestionsData.Select(x => new Question { Id = x.Id, QuestionText = x.QuestionText, Answers = x.Answers.Select(y => new Answer { AnswerText = y.AnswerText, Id = y.Id, QuestionId = x.Id }).ToList() }).ToList();
            
            return questions;
        }

        // PUT: api/Tests/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTest(int id, Test test)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != test.Id)
            {
                return BadRequest();
            }

            db.Entry(test).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tests
        [HttpPost]
        public object PostTest([FromBody] FullAnswer fullAnswer)
        {
            if (!ModelState.IsValid || fullAnswer == null)
            {
                return null;
            }

            AnswerItem[] answers = fullAnswer.Answers;

            int correct = 0, incorrect = 0;
            List<Answer> dbAnswers = db.Answers.ToList();
            int testId = 0;

            foreach (var answer in answers)
            {
                Answer currentAnswer = dbAnswers.Where(x => x.QuestionId == answer.Id && x.Id == answer.Value).First();                
                testId = db.Questions.Where(x => x.Id == answer.Id).First().TestId;

                if (currentAnswer.IsCorrect == true)
                    correct++;
                else
                    incorrect++;
            }

            User user = new User() { FirstName = fullAnswer.FirstName, SecondName = fullAnswer.SecondName, ThirdName = fullAnswer.LastName, Login = "temp", Password = "temp" };
            db.PassedTests.Add(new PassedTest { Date = DateTime.Now, CorrectAnswers = correct, NotCorrectAnswers = incorrect, IsCompleted = true, Test = db.Tests.Where(x => x.Id == testId).First(), User = user });
            db.SaveChanges();

            return new { Correct = correct, Incorrect = incorrect };
        }

        // DELETE: api/Tests/5
        [ResponseType(typeof(Test))]
        public async Task<IHttpActionResult> DeleteTest(int id)
        {
            Test test = await db.Tests.FindAsync(id);
            if (test == null)
            {
                return NotFound();
            }

            db.Tests.Remove(test);
            await db.SaveChangesAsync();

            return Ok(test);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TestExists(int id)
        {
            return db.Tests.Count(e => e.Id == id) > 0;
        }
    }
}