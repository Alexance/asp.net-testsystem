/**
 * Created by Дмитрий on 21.11.2014.
 */

var system = {
    tests : [],
    questions : [],
    // Данные о пользователе
    credentials : { },

    loadAllTests : function () {
        $.ajax({
            url : "/api/tests",
            type : "get",
            async : false,
            success : function (data) {
                if (!data)
                    return;

                system.tests = data;
                system.design.addTestsList(data);
            },
            error : function () {
                alert("Невозможно загрузить данные с сервера!");
            }
        });
    },

    loadAllQuestions : function (id) {
        system.selectedTest = id;

        $.ajax({
           url: "/api/tests/" + id,
            type : "get",
            async : false,
            success : function (data) {
                system.questions = data.slice();
            },
            error : function (data) {
                alert("Невозможно загрузить данные с сервера!");
            }
        });
    },

    sendResults : function (results) {
        $.ajax({
            url : "api/tests/",
            data : results,
            type : "post",
            async : false,
            success : function (data) {
                system.design.showAnswers(data);
            },
            error : function () {
                alert("Невозможно отправить данные на сервер!");
            }
        });
    }
}

system.design = {
    showAnswers : function (finalResults) {
        $("#board").off();
        $("#board").empty();

        var li = document.createElement("li");
        $(li).attr({"class" : "results-element"});

        var createH2 = function(className, text) {
            var h2 = document.createElement("h2");
            $(h2).attr({ "class" : className });
            $(h2).text(text);

            return h2;
        }

        var h2TestName = createH2("results-element-header", 'Тест: "' + system.tests[system.selectedTest - 1].name + '"');
        $(h2TestName).css({ "color" : "#22a5ed" });
        var h2CorrectAnswers = createH2("results-element-header", "Правильных ответов: " + finalResults.Correct);
        var h2IncorrectAnswers = createH2("results-element-header", "Неправильных ответов: " + finalResults.Incorrect);
        $(h2IncorrectAnswers).css({ "color" : "#c94d32" });
        var h2Percents = createH2("results-element-result results-element-test", "Процент правильных ответов: " + parseFloat(finalResults.Correct) / (parseFloat(finalResults.Correct) + parseFloat(finalResults.Incorrect)) * 100 + "%" );

        $(li).append([ h2TestName, h2CorrectAnswers, h2IncorrectAnswers, h2Percents ]);

        $("#board").append(li);
    },

    collectAnswers : function() {
        var results = [];
        var getGroupResult = function (name) {
            return $('input[name="' + name + '"]:checked').val();
        }

        for (var i = 0; i < system.questions.length; i++) {
            var result = getGroupResult("question-" + system.questions[i].Id);
            results.push({ Id : system.questions[i].Id, Value : parseInt(result) });
        }

        return results;
    },


   showTestQuestions : function () {
       $("#board").off();
       $("#board").empty();
       $(".column-header").first().text("Тест");

       var addQuestion = function (question) {
            var li = document.createElement("li");
            $(li).attr({ "class" : "results-element" });

            var h2 = document.createElement("h2");
            $(h2).attr({ "class" : "results-element-header" });
            $(h2).text(question.Id + ". " + question.QuestionText);
            $(li).append(h2);

            for (var i = 0; i < question.Answers.length; i++) {
                var radio = document.createElement("input");
                $(radio).attr({ "type" : "radio", "name" : "question-" + question.Id, "value" : question.Answers[i].Id });

                var h2Body = document.createElement("h2");
                $(h2Body).attr({ "class" : "results-element-test" });
                $(h2Body).append(radio);
                $(h2Body).append(" " + question.Answers[i].AnswerText);
                $(h2Body).append("<br/>");

                $(li).append(h2Body);
            }

           return li;
       }

       for (var i = 0; i < system.questions.length; i++)
            $("#board").append(addQuestion(system.questions[i]));

       var li = document.createElement("li");
       $(li).attr({ "class" : "results-element"});
       $(li).css({ "text-align" : "center" });
       var button = document.createElement("input");
       $(button).attr({ "class" : "accept-button", "type" : "button", "value" : "Закончить", "id" : "end-button" });
       $(li).append(button);

       $("#board").append(li);

       $("#end-button").on("click", function() {
            var results = system.design.collectAnswers();
            var sendData = {
                "FirstName" : system.credentials.first_name,
                "SecondName" : system.credentials.second_name,
                "LastName" : system.credentials.last_name,
                "Group" : system.credentials.group,
                Answers : results
            }

           system.sendResults(sendData);
       });
   },

    showUserInfo : function () {
        $("#board").off();
        $("#board").empty();

        var addNewField = function(header, name) {
            // Главный контейнер
            var li = document.createElement("li");
            $(li).attr({ "class" : "results-element" });

            // Заголовок
            var h2 = document.createElement("h2");
            $(h2).attr({ "class" : "results-element-header" });
            $(h2).text(header);

            // Поле для ввода
            var input = document.createElement("input");
            $(input).attr({ "type" : "text", "name" : name, "id" : name });

            $(li).append(h2);
            $(li).append(input);

            return li;
        }

        $("#board").append(addNewField("Фамилия", "first_name"));
        $("#board").append(addNewField("Имя", "second_name"));
        $("#board").append(addNewField("Отчество", "last_name"));
        $("#board").append(addNewField("Группа", "group"));

        var li = document.createElement("li");
        $(li).attr({ "class" : "results-element"});
        $(li).css({ "text-align" : "center" });
        var button = document.createElement("input");
        $(button).attr({ "class" : "accept-button", "type" : "button", "value" : "Начать тест", "id" : "start-button" });
        $(li).append(button);

        $("#board").append(li);
        $(".column-header").first().text("Анкета тестируемого");

        $("#start-button").on("click", function() {

            if ($("#first_name").val() == 0 || $("#second_name").val().length == 0 || $("#last_name").val().length == 0 || $("#group").val().length == 0) {
                alert("Заполните, пожалуйста, все поля!");
                return;
            }

            system.credentials.first_name = $("#first_name").val();
            system.credentials.second_name = $("#second_name").val();
            system.credentials.last_name = $("#last_name").val();
            system.credentials.group = $("#group").val();

            system.design.showTestQuestions();
        });
    },

    addTestsList : function (tests) {
        var addNewTest = function (test) {
            var header = "Тест " + test.id;
            var body = test.name;

            var createSubElement = function (text, className) {
                var h2 = document.createElement("h2");
                h2.className = className;
                h2.innerText = text;

                return h2;
            }

            var li = document.createElement("li");
            $(li).attr({ "class" : "results-element", "id" : test.id });

            var a = document.createElement("a");
            $(a).attr({ "class" : "element-link", "href" : "/#/" + test.id });
            $(a).append(createSubElement(header, "results-element-header"));
            $(a).append(createSubElement(body, "results-element-test"));

            $(li).append(a);

            $("#board").append(li);
        };

        for (var i = 0; i < tests.length; i++)
            addNewTest(tests[i]);

        $("#board").on("click", "li", function() {
            system.loadAllQuestions($(this).attr("id"));
            system.design.showUserInfo();
        });
    }


}

$(document).ready(function () {
    system.loadAllTests(); // Загрузить тесты с сервера
});