﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TestSystem.Models
{
    public class Answer
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(1024)]
        public string AnswerText { get; set; }
        public bool? IsCorrect { get; set; }

        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }
    }
}