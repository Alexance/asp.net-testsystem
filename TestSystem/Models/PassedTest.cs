﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TestSystem.Models
{
    public class PassedTest
    {
        [Key]
        public int Id { get; set; }

        public DateTime Date { get; set; }
        public int CorrectAnswers { get; set; }
        public int NotCorrectAnswers { get; set; }
        public bool IsCompleted { get; set; }

        public int UserId { get; set; }
        public int TestId { get; set; }

        public virtual User User { get; set; }
        public virtual Test Test { get; set; }
    }
}