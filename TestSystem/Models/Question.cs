﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TestSystem.Models
{
    public class Question
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(1024)]
        public string QuestionText { get; set; }

        public int TestId { get; set; }
        public virtual Test Test { get; set; }

        public virtual List<Answer> Answers { get; set; }
    }
}