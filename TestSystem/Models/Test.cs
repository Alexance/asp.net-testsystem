﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TestSystem.Models
{
    public class Test
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(256)]
        public string name { get; set; }

        public virtual List<Question> Questions { get; set; }
    }
}