﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSystem.Models.Transfers
{
    public class AnswerItem
    {
        public int Id { get; set; }
        public int Value { get; set; }
    }
}