﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSystem.Models.Transfers
{
    public class FullAnswer
    {
        public AnswerItem[] Answers { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string Group { get; set; }
    }
}