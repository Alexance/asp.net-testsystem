﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TestSystem.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(64)]
        public string FirstName { get; set; }
        [MaxLength(64)]
        public string SecondName { get; set; }
        [MaxLength(64)]
        public string ThirdName { get; set; }
        [MaxLength(64)]
        public string Login { get; set; }
        [MaxLength(32)]
        public string Password { get; set; }

        public virtual List<PassedTest> PassedTests { get; set; }
    }
}