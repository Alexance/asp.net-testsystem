using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

using TestSystem.Models;

namespace TestSystem
{
    public class TestContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<PassedTest> PassedTests { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
    }
}